#include <stdio.h>
#include "incfile.h"

const float dt=0.02;

void screen2x2(float *dana){
  printf("\n%.4f %.4f\n%.4f %.4f\n",dana[0],dana[1],dana[2],dana[3]);
}
void screen2x1(float *dana){
  printf("\n%.4f\n%.4f\n",dana[0],dana[1]);
}
void sum2x1(float *a1,float *a2,float *tab){
  tab[0]=a1[0]+a2[0];
  tab[1]=a1[1]+a2[1];
}
void sum2x2(float *a1,float *a2,float *tab){
  tab[0]=a1[0]+a2[0];
  tab[1]=a1[1]+a2[1];
  tab[2]=a1[2]+a2[2];
  tab[3]=a1[3]+a2[3];
}
void dif2x1(float *a1,float *a2,float *tab){
  tab[0]=a1[0]-a2[0];
  tab[1]=a1[1]-a2[1];
}
void dif2x2(float *a1,float *a2,float *tab){
  tab[0]=a1[0]-a2[0];
  tab[1]=a1[1]-a2[1];
  tab[2]=a1[2]-a2[2];
  tab[3]=a1[3]-a2[3];
}
void mul2x1(float *a1,float *a2,float *tab){
  tab[0]=a1[0]*a2[0]+a1[1]*a2[1];
  tab[1]=a1[2]*a2[0]+a1[3]*a2[1];
}
void mul2x2(float *a1,float *a2,float *tab){
  tab[0]=a1[0]*a2[0]+a1[1]*a2[2];
  tab[1]=a1[0]*a2[1]+a1[1]*a2[3];
  tab[2]=a1[2]*a2[0]+a1[3]*a2[2];
  tab[3]=a1[2]*a2[1]+a1[3]*a2[3];
}
void mul2x1c(float *a1,float a2,float *tab){
  tab[0]=a1[0]*a2;
  tab[1]=a1[1]*a2;
  tab[2]=a1[2]*a2;
  tab[3]=a1[3]*a2;
}
void mul2x2c(float *a1,float a2,float *tab){
  tab[0]=a1[0]*a2;
  tab[1]=a1[1]*a2;
  tab[2]=a1[2]*a2;
  tab[3]=a1[3]*a2;
}
void trans2x2(float *a,float *tab){
  tab[0]=a[0];
  tab[1]=a[2];
  tab[2]=a[1];
  tab[3]=a[3];
}

void div2x2(float *a1,float *a2,float *tab){
  float dev=a2[0]*a2[3]-a2[1]*a2[2];
  if(dev!=0){
    tab[0]=(a1[0]*a2[3]-a1[1]*a2[2])/dev;
    tab[1]=(a1[1]*a2[0]-a1[0]*a2[1])/dev;
    tab[2]=(a1[2]*a2[3]-a1[3]*a2[2])/dev;
    tab[3]=(a1[3]*a2[0]-a1[2]*a2[1])/dev;
  }
}

void ThePredictedState(float *Xk_1,float *Xkp){
  float A[4];
  A[0]=1;
  A[1]=dt;
  A[2]=0;
  A[3]=1;
  mul2x1(A,Xk_1,Xkp);
}

void ThePredictedCovariance(float *Pk_1,float *Pkp){
  float A[4];
  float AT[4];
  float tmp[4];
  A[0]=1;
  A[1]=dt;
  A[2]=0;
  A[3]=1;
  trans2x2(A,AT);
  mul2x2(A,Pk_1,tmp);
  mul2x2(tmp,AT,Pkp);
}

void rownanie2(float *Pkp,float *R,float *K){
  float tmp[4];
  sum2x2(Pkp,R,tmp);
  div2x2(Pkp,tmp,K);
}

void rownanie3(float *Xkp,float *Y,float *K,float *Xk){
  float tmp1[4];
  float tmp2[4];
  dif2x1(Y,Xkp,tmp1);
  mul2x1(K,tmp1,tmp2);
  sum2x1(Xkp,tmp2,Xk);
}

void rownanie4(float *Pkp,float *K,float *Pk){
  float diag[4];
  float tmp[4];
  diag[0]=1;
  diag[1]=0;
  diag[2]=0;
  diag[3]=1;
  dif2x1(diag,K,tmp);
  mul2x1(tmp,Pkp,Pk);
}

void rownanie5(float *Xk,float *Pk,float *Xk_1,float *Pk_1){
  Xk_1[0]=Xk[0];
  Xk_1[1]=Xk[1];
  Pk_1[0]=Pk[0];
  Pk_1[1]=Pk[1];
  Pk_1[2]=Pk[2];
  Pk_1[3]=Pk[3];
}

int main(){
  float Xk_1[2];
  float Xkp[2];
  float Xk[2];
  float Y[2];
  float Pk_1[2][2];
  float Pkp[2][2];
  float Pk[2][2];
  float R[2][2];
  float K[2][2];
  Pk_1[0][0]=1.23;
  Pk_1[0][1]=1.24;
  Pk_1[1][0]=1.25;
  Pk_1[1][1]=1.26;
  R[0][0]=4.64;
  R[0][1]=4.65;
  R[1][0]=4.66;
  R[1][1]=4.67;
  Xk_1[0]=2.56;
  Xk_1[1]=2.57;
  Y[0]=0.11;
  Y[1]=0.12;
  printf("Xk_1:");
  screen2x1(Xk_1);
  printf("Y:");
  screen2x1(Y);
  printf("Pk_1:");
  screen2x2(*Pk_1);
  printf("R:");
  screen2x2(*R);
  ThePredictedState(Xk_1,Xkp);
  ThePredictedCovariance(*Pk_1,*Pkp);
  rownanie2(*Pkp,*R,*K);
  rownanie3(Xkp,Y,*K,Xk);
  rownanie4(*Pkp,*K,*Pk);
  rownanie5(Xk,*Pk,Xk_1,*Pk_1);
  printf("\nPo obliczeniach\n\n");
  printf("Xk_1:");
  screen2x1(Xk_1);
  printf("Xkp:");
  screen2x1(Xkp);
  printf("Xk:");
  screen2x1(Xk);
  printf("Y:");
  screen2x1(Y);
  printf("Pk_1:");
  screen2x2(*Pk_1);
  printf("Pkp:");
  screen2x2(*Pkp);
  printf("Pk:");
  screen2x2(*Pk);
  printf("R:");
  screen2x2(*R);
  printf("K:");
  screen2x2(*K);
  return 0;
}
