#include<iostream>
#include <string>
#include <iomanip>
#include <ctime>
//#include <chrono>
#include<cstdlib>
#include <stack>
#include <queue>
#include "kolejka.hh"

using namespace std;

bool wyswietl(List<int> &dane);

int dl_tab=0;
int dl_tab_x2=0;

int main(int argc, char *argv[]){
  List<int> stos;                            // korzen listy
  int *stos_tab;
  int *stos_tab_x2;          // zmienna pomocnicza do tablic dynamicznych
  int *tmp_tab=NULL;
  int c=1000;
  clock_t begin;              // zmienna od poczatku pomiaru
  clock_t end;                // zmienna od konica pomiaru
  double elapsed_secs;
  int powtorzenia[4]={1000,10000,100000,500000}; // zbior powtorzen operacji
  srand(time(NULL)); // ustalenie punktu poczatkowego w danym monencie czasu procesora
  for(int j=0;j<4;j++){       // wykonanie dla wszystkich grup n
    
    begin=clock();                        // poczatek pomiaru
    for(int i=0;i<powtorzenia[j];i++){    // alokacja n elementow
      stos.dodaj(std::rand()%(100));      // dodawanie do stosu
    }
    end=clock();                          // koniec pomiaru
    stos.clear();                         // czyszczenie stosu
    elapsed_secs=double(end - begin)/CLOCKS_PER_SEC;
    cout<<"Time stos        "<<powtorzenia[j]<<" elementow: "<<elapsed_secs<<endl;
            
    begin=clock();
    for(long int nr=0;nr<powtorzenia[j];nr++){    // alokowoanie elementow
      if(nr<dl_tab){                              // gdy na tablicy jeszcze jest miejsce
	stos_tab[nr]=std::rand()%(100);           // nadanie losowej wartosci
      }else if(nr==dl_tab){                       // gdy na tablicy brakuje miejsca
	tmp_tab=new int[dl_tab];                  // pomocnicza tablica
	for(long int k=0;k<dl_tab;k++)tmp_tab[k]=stos_tab[k];  // przepisanie tablicy do tablicy pomocniczej
	if(dl_tab)delete [] stos_tab;                          // usuniecie tablicy
	stos_tab=new int[dl_tab+c];                            // zaalokowanie większej tablicy
	for(long int k=0;k<dl_tab;k++)stos_tab[k]=tmp_tab[k];  // przepisanie spowrotem danych z tablicy pomocniczej
	delete [] tmp_tab;                                     // usuniecie tablicy pomocniczej
	stos_tab[nr]=std::rand()%(100);                        // nadanie losowaj wartosci kolejnemu elementowi
	dl_tab=dl_tab+c;                                       // zapisanie rozmiaru tablicy
      }else{                                                   // gdy wystapi blad podczas liczenia kolejnych elementow 
	cout<<"Mamy blad"<<endl;
      }
    }
    end=clock();
    delete [] stos_tab;                                        // usuniecie tablicy danych
    dl_tab=0;
    elapsed_secs=double(end - begin)/CLOCKS_PER_SEC;
    cout<<"Time stos tab    "<<powtorzenia[j]<<" elementow: "<<elapsed_secs<<endl;
    
    begin=clock();  
    for(long int nr=0;nr<powtorzenia[j];nr++){
      if(nr<dl_tab){
	stos_tab_x2[nr]=std::rand()%(100);;
      }else if(nr==dl_tab){
	tmp_tab=new int[dl_tab];
	for(long int k=0;k<dl_tab;k++){
	  tmp_tab[k]=stos_tab_x2[k];
	}
	if(dl_tab)delete [] stos_tab_x2;
	if(dl_tab){
	  stos_tab_x2=new int[2*dl_tab];
	  //	  cout<<"dl_tab: "<<2*dl_tab<<endl;
	}else{stos_tab_x2=new int[1];}
	for(long int k=0;k<dl_tab;k++){
	  stos_tab_x2[k]=tmp_tab[k];
	}
	delete [] tmp_tab;
	stos_tab_x2[nr]=std::rand()%(100);
	if(dl_tab)dl_tab=2*dl_tab;
	else dl_tab=1;
	//	cout<<"dl_tab: "<<dl_tab<<endl;
      }else{
	cout<<"Mamy blad"<<endl;
      }
    }
    end=clock();
    delete [] stos_tab_x2;
    dl_tab=0;
    elapsed_secs=double(end - begin)/CLOCKS_PER_SEC;
    cout<<"Time stos tab x2 "<<powtorzenia[j]<<" elementow: "<<elapsed_secs<<endl;

    cout<<endl;
  }
  //  cout<<"dl: "<<stos.rozmiar()<<endl<<"Is empty: "<<stos.isEmpty()<<endl;
  return 0;
}

// wyswietlanie zawartosci stosow i kolejek

bool wyswietl(List<int> &dane){
  List<int> tmp;
  int rozmiar=dane.rozmiar();
  if(dane.isEmpty()){
    cout<<endl<<"Stos jest pusty"<<endl;
    return false;
  }
  cout<<"Zawartosc stosu: ";
  for(int i=0;i<rozmiar;i++){
    cout<<dane.pierwszy()<<' ';
    tmp.dodaj(dane.usun());
  }
  cout<<endl;
  for(int i=0;i<rozmiar;i++){
    dane.dodaj(tmp.usun());
  }
  tmp.clear();
  return true;
}
