#include <iostream>
#include <string>
#include<cstdlib>
#include<ctime>
#include <stack>
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;
bool wyswietl(List<int> &dane);
bool wyswietl(stack<int> &dane);
void clearStack(stack<int> &stos){while(!(stos.empty()))stos.pop();}

int main(int argc, char *argv[]){
  stack<int> stos_stl;
  List<int> stos;                               // stos w oparciu o liste
  char wybor=0;                                 // zmienna wyboru
  int tmp;
  while(1){
    cout<<endl;                                 // menu wyboru
    cout<<"Obsluga stosow danych."<<endl;
    cout<<"Wybierz co chcesz robic:"<<endl;
    cout<<"1. Dodaj do stosu element"<<endl;
    cout<<"2. Wyswietl stos"<<endl;
    cout<<"3. Usun z stosu element"<<endl;
    cout<<"4. Wyczysc stos"<<endl;
    cout<<"5. Dodaj do stosu stl element"<<endl;
    cout<<"6. Wyswietl stos stl"<<endl;
    cout<<"7. Usun z stosu stl element"<<endl;
    cout<<"8. Wyczysc stos stl"<<endl;
    cout<<"q. zamknij"<<endl;
    cin>>wybor;
    switch(wybor){
    case '1':
      cout<<"Podaj wartosc: ";
      cin>>tmp;
      stos.dodaj(tmp);
      break;
    case '2':
      wyswietl(stos);
      break;
    case '3':
      try{
	cout<<"Usunieto: "<<stos.usun()<<endl;
      }catch(ListEmptyException){
	cout<<"Stos jest pusty"<<endl;
      }
      break;
    case '4':
      stos.clear();
      cout<<endl<<"Stos zostal oprozniony"<<endl;
      break;
    case '5':
      cout<<"Podaj wartosc: ";
      cin>>tmp;
      stos_stl.push(tmp);
      break;
    case '6':
      wyswietl(stos_stl);
      break;
    case '7':
      if(!(stos_stl.empty())){
	cout<<"Usunieto: "<<stos_stl.top()<<endl;
	stos_stl.pop();
      }else{
	cout<<"Stos stl jest pusty"<<endl;
      }
      break;
    case '8':
      clearStack(stos_stl);
      cout<<endl<<"Stos stl zostal oprozniony"<<endl;
      break;
    case 'q':
      stos.clear(); // zwalnianie pamieci przed zamknieciem
      clearStack(stos_stl);
      //      cout<<"dl: "<<stos.rozmiar()<<endl<<"Is empty: "<<stos.isEmpty()<<endl;
      //      cout<<"Wyciek pamieci: "<<dl_kolejki<<endl;
      return 0;
      break;
    default:
      cout<<"Zle okreslony wybor"<<endl;
      break;
    }
  }
}

bool wyswietl(List<int> &dane){
  List<int> tmp;
  int rozmiar=dane.rozmiar();
  if(dane.isEmpty()){
    cout<<endl<<"Stos jest pusty"<<endl;
    return false;
  }
  cout<<"Zawartosc stosu: ";
  for(int i=0;i<rozmiar;i++){    // przelozenie elementow na stos pomocniczy
    cout<<dane.pierwszy()<<' ';  // wyswietlenie
    tmp.dodaj(dane.usun());
  }
  cout<<endl;
  for(int i=0;i<rozmiar;i++){    // przelozenie elementow spowrotem
    dane.dodaj(tmp.usun());
  }
  tmp.clear();
  return true;
}

bool wyswietl(stack<int> &dane){
  stack<int> tmp;
  int rozmiar=dane.size();
  if(dane.empty()){
    cout<<endl<<"Stos stl jest pusty"<<endl;
    return false;
  }
  cout<<"Zawartosc stosu stl: ";
  for(int i=0;i<rozmiar;i++){    // przelozenie elementow na stos pomocniczy
    cout<<dane.top()<<' ';       // wyswietlenie
    tmp.push(dane.top());
    dane.pop();
  }
  cout<<endl;
  for(int i=0;i<rozmiar;i++){    // przelozenie elementow spowrotem
    dane.push(tmp.top());
    tmp.pop();
  }
  clearStack(tmp);
  return true;
}
