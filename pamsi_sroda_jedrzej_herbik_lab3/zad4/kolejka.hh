class ListEmptyException{};
extern int dl_kolejki; // zmienna do okreslania przecieku pamieci

template <class Object>
class wezelListy{
protected:
  Object zawartosc;    // dane
  wezelListy<Object> *next; // wskaznik do przodu
public:
  wezelListy();
  ~wezelListy();
  Object& getZawartosc();
  Object& setZawartosc(const Object &dane);
  wezelListy<Object>*& getNext();
  wezelListy<Object>*& setNext(wezelListy<Object> (*dane));
};

template <class Object>
Object& wezelListy<Object>::getZawartosc(){return zawartosc;}

template <class Object>
Object& wezelListy<Object>::setZawartosc(const Object &dane){zawartosc=dane;return zawartosc;}

template <class Object>
wezelListy<Object>*& wezelListy<Object>::getNext(){return next;}

template <class Object>
wezelListy<Object>*& wezelListy<Object>::setNext(wezelListy<Object> (*dane)){next=dane;return next;}

template <class Object>
wezelListy<Object>::~wezelListy(){dl_kolejki--;}

template <class Object>
wezelListy<Object>::wezelListy(){next=NULL;dl_kolejki++;}

template <class Object> // szablon listy
class List{
protected:
  Object zawartosc;    // dane
  wezelListy<Object> *head; // wskaznik do przodu  
  int dl;
public:
  List();
  int rozmiar() const;
  bool isEmpty() const; 
  bool clear();         // wyczysc
  Object& pierwszy() throw(ListEmptyException);
  Object& dodaj(const Object& obj);
  Object& usun() throw(ListEmptyException);
};

template <class Object>
List<Object>::List(){
  head=NULL;
  dl=0;
}

template <class Object>
int List<Object>::rozmiar() const{
  return dl;
}

template <class Object>
bool List<Object>::isEmpty() const{
  if(dl) return false;
  return true;
}

template <class Object>
bool List<Object>::clear(){
  wezelListy<Object> *tmp=NULL;
  while(head!=NULL){ // dopoki sa elementy
    tmp=head->getNext();  // przejscie na nastepne elementy
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl; // kontrola dzialania funkcji
    delete(head); // zaolnienie pamieci
    head=tmp; // przesuniecie korzenia
    dl=0;
  }
  if(head==NULL){
    //    std::cout<<"Lista jest pusta"<<std::endl; // kontrola dzialania funkcji
    return true;
  }
  return false;
}

template <class Object>
Object& List<Object>::pierwszy() throw(ListEmptyException){
  if(head!=NULL){
    return head->getZawartosc(); // zwrocenie wartosci pierwszego elementu
  }else{
    //cout<<"Lista jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu
  }
}

template <class Object>
Object& List<Object>::dodaj(const Object& obj){
  wezelListy<Object> *tmp=NULL;
  tmp=head;
  head=new wezelListy<Object>;  // alokacja nowej komorki
  head->setZawartosc(obj);      // inicjalizacja komorki
  dl++;    
  head->setNext(tmp);           // dolozenie do listy
  return head->getZawartosc();
}

template <class Object>
Object& List<Object>::usun() throw(ListEmptyException){
  wezelListy<Object> *tmp=NULL;
  if(head!=NULL){ // gdy lista zawiera elementy
    tmp=head->getNext();
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl;
    zawartosc=head->getZawartosc();
    delete(head); // zwolnienie pamieci
    head=tmp;
    dl--;
    return zawartosc;
  }else{ // gdy lista pusta
    //cout<<"Lista jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu 
  }
}

class KolejkaEmptyException{};

template <class Object> // szablon kolejki
class Kolejka: public List<Object>{
  wezelListy<Object> *begin; // wskaznik do przodu  
  wezelListy<Object> *end; // wskaznik do przodu  
public:
  Object& dodaj(const Object& obj);
  Object& usun() throw(KolejkaEmptyException);
};

template <class Object>
Object& Kolejka<Object>::usun() throw(KolejkaEmptyException){
  wezelListy<Object> *tmp=NULL;
  if(this->head!=NULL){ // gdy kolejka zawiera elementy
    tmp=this->head->getNext();
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl;
    this->zawartosc=this->head->getZawartosc();
    delete(this->head); // zwolnienie pamieci
    this->head=tmp;
    this->dl--;
    if(this->head==NULL)end=NULL;
    return this->zawartosc;
  }else{ // gdy kolejka pusta
    //cout<<"Kolejka jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu 
  }
}

template <class Object>
Object& Kolejka<Object>::dodaj(const Object& obj){
  wezelListy<Object> *tmp=new wezelListy<Object>;
  tmp->setZawartosc(obj);
  if(this->head==NULL){ // gdy kolejka pusta
    this->head=tmp;
  }else{          // gdy kolejka zawiera elementy
    end->setNext(tmp);
  }
  end=tmp; // wprzypisanie pamieci do wskaznika koncowego
  this->dl++;
  return end->getZawartosc();
}
