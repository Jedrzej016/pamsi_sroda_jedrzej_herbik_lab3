#include <iostream>
#include <string>
#include <cstdlib>
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;
bool wyswietl(List<int> &A,List<int> &B,List<int> &C);
void hanoi(int lk,List<int> &A,List<int> &B,List<int> &C,List<int> &stosA,List<int> &stosB,List<int> &stosC);

int main(int argc, char *argv[]){
  List<int> stosA;
  List<int> stosB;
  List<int> stosC;
  int lk=0; // Liczba krążków
  cout<<"Podaj liczbe elementow: ";
  cin >> lk;
  for(int i=0;i<lk;i++){
    stosA.dodaj(lk-i);
  }
  cin.get();system("clear");
  wyswietl(stosA,stosB,stosC);
  hanoi(lk,stosA,stosB,stosC,stosA,stosB,stosC); // Wywołanie funkcji "hanoi".
  stosA.clear();
  stosB.clear();
  stosC.clear();
  //  cout<<"Wyciek pamieci: "<<dl_kolejki<<" "<<stosA.rozmiar()<<" "<<stosB.rozmiar()<<" "<<stosC.rozmiar()<<endl;
  return 0;
}

void hanoi(int lk,List<int> &A,List<int> &B,List<int> &C,List<int> &stosA,List<int> &stosB,List<int> &stosC){  // wywolanie dostaje rekurence do stosow by moc na nich pracowac oraz druga by moc je wyswietlac 
  if(lk>0){
    hanoi(lk-1,A,C,B,stosA,stosB,stosC); // Przekłada lk-1 krążków ze źródłowego stosu (A) na pomocniczy (B) przy użyciu stosu docelowego (C).
    try{C.dodaj(A.usun());}catch(ListEmptyException){cout<<"Nie poszlo"<<endl;}  // przelozenie elementu miedzy stosami
    //    cin.get();system("clear");  // kazdy z krokow wyswietla sie enterem (nie polecam dla wiekszych wierz niz 6)
    wyswietl(stosA,stosB,stosC);         // wyswietlenie kroku
    hanoi(lk-1,B,A,C,stosA,stosB,stosC); // Przekłada lk-1 krążków z pomocniczego stosu (B) na docelowy (C) przy użyciu stosu źródłowego (A).
  }

}

bool wyswietl(List<int> &A,List<int> &B,List<int> &C){
  List<int> tmpA;  // pomocnicze listy do przechowywanie elementow zdjetych
  List<int> tmpB;
  List<int> tmpC;
  int rozmiar=A.rozmiar();
  if(B.rozmiar()>rozmiar)rozmiar=B.rozmiar();
  if(C.rozmiar()>rozmiar)rozmiar=C.rozmiar();  // wybranie wielkosci maksymalnej
  if((A.isEmpty())&&(B.isEmpty())&&(C.isEmpty())){
    cout<<endl<<"Stos jest pusty"<<endl;
    return false;
  }
  cout<<"Zawartosc stosu"<<endl;  // ramka graficzna
  cout<<" A | B | C "<<endl;
  cout<<"---|---|---"<<endl;
  cout<<"   |   |   "<<endl;
  for(int i=0;i<rozmiar;i++){
    if((rozmiar-i)==A.rozmiar()){
      if(A.pierwszy()>99){               // funkcje do zachowania wymiaru ramki oraz wyswietlenia
	cout<<A.pierwszy()<<"|";
      }else if(A.pierwszy()>9){
	cout<<" "<<A.pierwszy()<<"|";
      }else{
	cout<<" "<<A.pierwszy()<<" |";
      }
      tmpA.dodaj(A.usun());              // zdjecie elementu wyswietlonego
    }else cout<<"   |";
    
    if((rozmiar-i)==B.rozmiar()){        // powtorzenie dla stosu B oraz C
      if(B.pierwszy()>99){
	cout<<B.pierwszy()<<"|";
      }else if(B.pierwszy()>9){
	cout<<" "<<B.pierwszy()<<"|";
      }else{
	cout<<" "<<B.pierwszy()<<" |";
      }
      tmpB.dodaj(B.usun());
    }else cout<<"   |";
    
    if((rozmiar-i)==C.rozmiar()){
      if(C.pierwszy()>99){
	cout<<C.pierwszy();
      }else if(C.pierwszy()>9){
	cout<<" "<<C.pierwszy();
      }else{
	cout<<" "<<C.pierwszy();
      }
      tmpC.dodaj(C.usun());
    }else cout<<"   ";
    cout<<endl;
  }
  cout<<endl;
  for(int i=0;i<rozmiar;i++){                 // zwrocenie zawartosci na stosy
    try{A.dodaj(tmpA.usun());}catch(ListEmptyException){}
    try{B.dodaj(tmpB.usun());}catch(ListEmptyException){}
    try{C.dodaj(tmpC.usun());}catch(ListEmptyException){}
  }
  tmpA.clear();  // awaryjne zwolnienie pamieci
  tmpB.clear();
  tmpC.clear();
  return true;
}
