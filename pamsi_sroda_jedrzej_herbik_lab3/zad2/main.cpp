#include<iostream>
#include <string>
#include<cstdlib>
#include<ctime>
#include <queue>
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;
bool wyswietl(List<int> &dane);
bool wyswietl(queue<int> &dane);
void clearQueue(queue<int> &kolejka){while(!(kolejka.empty()))kolejka.pop();}

int main(int argc, char *argv[]){
  queue<int> kolejka_stl;
  Kolejka<int> kolejka;                            // korzen kolejki
  char wybor=0;                                 // zmienna wyboru
  int tmp;
  while(1){
    cout<<endl;                                 // menu wyboru
    cout<<"Obsluga kolejek danych."<<endl;
    cout<<"Wybierz co chcesz robic:"<<endl;
    cout<<"1. Dodaj do kolejki element"<<endl;
    cout<<"2. Wyswietl kolejke"<<endl;
    cout<<"3. Usun z kolejki element"<<endl;
    cout<<"4. Wyczysc kolejke"<<endl;
    cout<<"5. Dodaj do kolejki stl element"<<endl;
    cout<<"6. Wyswietl kolejke stl"<<endl;
    cout<<"7. Usun z kolejki stl element"<<endl;
    cout<<"8. Wyczysc kolejke stl"<<endl;
    cout<<"q. zamknij"<<endl;
    cin>>wybor;
    switch(wybor){
    case '1':
      cout<<"Podaj wartosc: ";
      cin>>tmp;
      kolejka.dodaj(tmp);
      break;
    case '2':
      wyswietl(kolejka);
      break;
    case '3':
      try{
	cout<<"Usunieto: "<<kolejka.usun()<<endl;
      }catch(KolejkaEmptyException){
	cout<<"Kolejka jest pusta"<<endl;
      }
      break;
    case '4':
      kolejka.clear();
      cout<<endl<<"Kolejka zostala oprozniona"<<endl;
      break;
    case '5':
      cout<<"Podaj wartosc: ";
      cin>>tmp;
      kolejka_stl.push(tmp);
      break;
    case '6':
      wyswietl(kolejka_stl);
      break;
    case '7':
      if(!(kolejka_stl.empty())){
	cout<<"Usunieto: "<<kolejka_stl.front()<<endl;
	kolejka_stl.pop();
      }else{
	cout<<"Kolejka stl jest pusta"<<endl;
      }
      break;
    case '8':
      clearQueue(kolejka_stl);
      cout<<endl<<"Kolejka stl zostala oprozniona"<<endl;
      break;
    case 'q':
      kolejka.clear(); // zwalnianie pamieci przed zamknieciem
      clearQueue(kolejka_stl);
      //      cout<<"dl: "<<kolejka.rozmiar()<<endl<<"Is empty: "<<kolejka.isEmpty()<<endl;
      //      cout<<"Wyciek pamieci: "<<dl_kolejki<<endl;
      return 0;
      break;
    default:
      cout<<"Zle okreslony wybor"<<endl;
      break;
    }
  }
}

bool wyswietl(List<int> &dane){
  List<int> tmp;
  int rozmiar=dane.rozmiar();
  if(dane.isEmpty()){
    cout<<endl<<"Kolejka jest pusta"<<endl;
    return false;
  }
  cout<<"Zawartosc kolejki: ";
  for(int i=0;i<rozmiar;i++){                   // przelozenie elementow na kolejke pomocnicza
    cout<<dane.pierwszy()<<' ';                 // wyswietlenie
    tmp.dodaj(dane.usun());
  }
  cout<<endl;
  for(int i=0;i<rozmiar;i++){                   // przelozenie elementow spowrotem
    dane.dodaj(tmp.usun());
  }
  tmp.clear();
  return true;
}

bool wyswietl(queue<int> &dane){
  queue<int> tmp;
  int rozmiar=dane.size();
  if(dane.empty()){
    cout<<endl<<"Kolejka stl jest pusta"<<endl;
    return false;
  }
  cout<<"Zawartosc kolejki stl: ";
  for(int i=0;i<rozmiar;i++){                   // przelozenie elementow na kolejke pomocnicza
    cout<<dane.front()<<' ';                    // wyswietlenie
    tmp.push(dane.front());
    dane.pop();
  }
  cout<<endl;
  for(int i=0;i<rozmiar;i++){                   // przelozenie elementow spowrotem
    dane.push(tmp.front());
    tmp.pop();
  }
  clearQueue(tmp);
  return true;
}
